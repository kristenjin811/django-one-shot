from django.shortcuts import render
from django.urls import reverse_lazy
from .models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/create.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/edit.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todos/task-create.html"

    def get_success_url(self):
        return reverse_lazy(
          "todo_list_detail",
          args=[self.object.list.id]
        )


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/task-update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
          "todo_list_detail",
          args=[self.object.list.id]
        )
